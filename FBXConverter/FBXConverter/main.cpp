#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <fstream>
#include <iterator>
#include "StreamConverter.h"
#include "AnimationConverter.h"
using namespace std;


#define CHANGE_BINARY(data) reinterpret_cast<const char*>(&data)
#define CHANGE_FLOAT(data) reinterpret_cast<const char*>(&data)
// output.write(CHANGE_BINARY(p.Pos.x),sizeof(p.Pos.x));

struct MainVertex
{
	DirectX::XMFLOAT3 Pos;
	DirectX::XMFLOAT3 Normal;
	DirectX::XMFLOAT2 TexC;
};

StreamConverter g_converter;
AnimationConverter g_AnimationConverter;

void ConvertFolder(string folderpath);
void ConvertBinaryFile(string path);
void ConvertFile(string path);

void AnimationConvertFolder(string folderpath);
void AnimationConvertFile(string path);

void ConvertFolder(string folderpath)
{
	string path;
	path = folderpath + "itemlist.txt";

	ifstream fin(path);

	if (!fin)
	{
		cout << "the path not found." << endl;
		return;
	}

	std::string token;
	string val;
	std::vector<std::string> fbxNames;

	int i = 0;
	while (!fin.eof())
	{
		fin >> val;
		val = folderpath + val;
		fbxNames.push_back(val);
		++i;
	}
	fin.close();

	int t = 0;
	for (auto& p : fbxNames) {
		ConvertFile(p.c_str());
		t++;
	}
}

void AnimationConvertFolder(string folderpath)
{
	string path;
	path = folderpath + "CharacterAnimationList.txt";

	ifstream fin(path);

	if (!fin)
	{
		cout << "the path not found" << endl;
		return;
	}

	string token;
	string val;
	vector<string> fbxNames;

	int i = 0;
	while (!fin.eof())
	{
		fin >> val;
		val = folderpath + val;
		fbxNames.push_back(val);
		++i;
	}
	fin.close();

	int t = 0;
	for (auto& p : fbxNames)
	{
		AnimationConvertFile(p.c_str());
		t++;
	}

}

void ConvertFile(string path)
{
	g_converter.ClearBuffer();
	g_converter.LoadFBX(path.c_str());

	string outputName;
	outputName = path;

	for (int i = 0; i < 4; ++i)
	{
		outputName.pop_back();
	}

	outputName += ".bin";

	if (g_converter.GetVertices().size() > 9000) {
		cout << outputName << endl;
		cout << g_converter.GetVertices().size() << endl;
		cout << g_converter.GetIndices().size() << endl;
	}


	ofstream output(outputName, ios::binary);

	output << "VertexCount: " << g_converter.GetVertices().size() << endl;
	output << "IndexCount: " << g_converter.GetIndices().size() << endl;

	output << "<VertexList>" << endl;
	for (auto& p : g_converter.GetVertices())
	{
		output << p.Pos.x << " " << p.Pos.y << " " << p.Pos.z << " ";
	}
	output << endl;

	output << "<NormalList>" << endl;
	for (auto& p : g_converter.GetVertices())
	{
		output << p.Normal.x << " " << p.Normal.y << " " << p.Normal.z << " ";
	}
	output << endl;

	output << "<TexCList>" << endl;
	for (auto& p : g_converter.GetVertices())
	{
		output << p.TexC.x << " " << p.TexC.y << " ";
	}
	output << endl;

	output << "<IndexList>" << endl;
	for (auto& p : g_converter.GetIndices())
	{
		output << p << " ";
	}
	output.close();
}

void AnimationConvertFile(string path)
{
	g_AnimationConverter.LoadFBX(path.c_str());

	string outputName;
	outputName = path;

	for (int i = 0; i < 4; ++i)
	{
		outputName.pop_back();
	}

	outputName += ".txt";

	ofstream output(outputName);

	output << "***************File-Header***************" << endl;
	output << "#Materials " << g_AnimationConverter.materials.size() << endl;
	output << "#Vertices " << g_AnimationConverter.Vertices.size() << endl;
	output << "#Triangles " << g_AnimationConverter.Indices.size() / 3 << endl;
	output << "#Bones " << g_AnimationConverter.ResultBoneOffsets.size() - 1 << endl;
	output << "#AnimationClips " << g_AnimationConverter.animationName.size() << endl;
	output << endl;

	output << "***************Materials*********************" << endl;
	for (int i = 0; i < g_AnimationConverter.materials.size(); ++i)
	{
		output << "Name: " << g_AnimationConverter.materials[i].name << endl;
		output << "Diffuse: " << g_AnimationConverter.materials[i].diffuse.x << g_AnimationConverter.materials[i].diffuse.y << g_AnimationConverter.materials[i].diffuse.z << endl;
		output << "Fresnel0: " << g_AnimationConverter.materials[i].fresnel0.x << g_AnimationConverter.materials[i].fresnel0.y << g_AnimationConverter.materials[i].fresnel0.z << endl;
		output << "Roughness: " << g_AnimationConverter.materials[i].roughness << endl;
		output << "AlphaClip: " << g_AnimationConverter.materials[i].alphaClip << endl;
		output << "MaterialTypeName: " << g_AnimationConverter.materials[i].materialTypeName << endl;
		output << "DiffuseMap: " << g_AnimationConverter.materials[i].diffusemap << endl;
		output << "NormalMap: " << g_AnimationConverter.materials[i].NormalMap << endl;
	}
	output << endl;

	output << "***************SubsetTable*******************" << endl;
	for (int i = 0; i < g_AnimationConverter.subsetTable.size(); ++i)
	{
		output << "SubsetID: " << i
			<< " VertexStart: " << g_AnimationConverter.subsetTable[i].vertexStart << " VertexCount: " << g_AnimationConverter.subsetTable[i].vertexCount
			<< " FaceStart: " << g_AnimationConverter.subsetTable[i].faceStart << " FaceCount: " << g_AnimationConverter.subsetTable[i].faceCount << endl;
	}
	output << endl;

	output << "***************Vertices**********************" << endl;
	for (auto& d : g_AnimationConverter.Vertices)
	{
		output << "Position: " << -d.Pos.x << " " << d.Pos.y << " " << d.Pos.z << endl;
		output << "Tangent: " << d.Tangent.x << " " << d.Tangent.y << " " << d.Tangent.z << " " << d.Tangent.w << endl;
		output << "Normal: " << d.Normal.x << " " << d.Normal.y << " " << d.Normal.z << endl;
		output << "Tex-Coords: " << d.TexC.x << " " << d.TexC.y << endl;
		output << "BlendWeights: " << d.BlendWeight.x << " " << d.BlendWeight.y << " " << d.BlendWeight.z << " " << d.BlendWeight.w << endl;
		output << "BlendIndices: " << d.BlendIndices.x << " " << d.BlendIndices.y << " " << d.BlendIndices.z << " " << d.BlendIndices.w << endl;
		output << endl;
	}


	output << "***************Triangles*********************" << endl;
	for (int i = 0; i < g_AnimationConverter.Indices.size(); i += 3)
	{
		output << g_AnimationConverter.Indices[i] << " " << g_AnimationConverter.Indices[i + 2] << " " << g_AnimationConverter.Indices[i + 1] << endl;
	}
	output << endl;


	output << "***************BoneOffsets*******************" << endl;
	for (int i = 0; i < g_AnimationConverter.ResultBoneOffsets.size() - 1; ++i)
	{
		output << g_AnimationConverter.HierarchyBone[i] << " "
			<< g_AnimationConverter.ResultBoneOffsets[i].m[0][0] << " " << g_AnimationConverter.ResultBoneOffsets[i].m[0][1] << " "
			<< g_AnimationConverter.ResultBoneOffsets[i].m[0][2] << " " << g_AnimationConverter.ResultBoneOffsets[i].m[0][3] << " "
			<< g_AnimationConverter.ResultBoneOffsets[i].m[1][0] << " " << g_AnimationConverter.ResultBoneOffsets[i].m[1][1] << " "
			<< g_AnimationConverter.ResultBoneOffsets[i].m[1][2] << " " << g_AnimationConverter.ResultBoneOffsets[i].m[1][3] << " "
			<< g_AnimationConverter.ResultBoneOffsets[i].m[2][0] << " " << g_AnimationConverter.ResultBoneOffsets[i].m[2][1] << " "
			<< g_AnimationConverter.ResultBoneOffsets[i].m[2][2] << " " << g_AnimationConverter.ResultBoneOffsets[i].m[2][3] << " "
			<< g_AnimationConverter.ResultBoneOffsets[i].m[3][0] << " " << g_AnimationConverter.ResultBoneOffsets[i].m[3][1] << " "
			<< g_AnimationConverter.ResultBoneOffsets[i].m[3][2] << " " << g_AnimationConverter.ResultBoneOffsets[i].m[3][3] << endl;
	}
	output << endl;

	output << "***************BoneHierarchy*****************" << endl;
	for (int i = 0; i < g_AnimationConverter.ParentIndex.size(); ++i)
	{
		output << "ParentIndexof" << g_AnimationConverter.HierarchyBone[i] << ": " << g_AnimationConverter.ParentIndex[i] << endl;
	}
	output << endl;


	output << "***************AnimationClips****************" << endl;
	for (int i = 0; i < g_AnimationConverter.animationName.size(); ++i)
	{
		output << "AnimationClip " << "Take001" << endl;//g_AnimationConverter.animationName[i] << endl;
		output << "{" << endl;
		for (int j = 0; j < g_AnimationConverter.HierarchyBone.size() -1; ++j)
		{
			output << "\t" << g_AnimationConverter.HierarchyBone[j] << " #Keyframes: " << g_AnimationConverter.AnimationFrameDataVector[j].keyTime.size() << endl;
			output << "\t" << "{" << endl;

			int count = 0;
			for (auto& d : g_AnimationConverter.AnimationFrameDataVector[j].keyTime)
			{
				output << "\t" << "\t" << "Time: " << d
					<< " Pos: " << g_AnimationConverter.AnimationFrameDataVector[j].position[count].x << " "
					<< g_AnimationConverter.AnimationFrameDataVector[j].position[count].y << " "
					<< g_AnimationConverter.AnimationFrameDataVector[j].position[count].z << " "
					<< " Scale: " << g_AnimationConverter.AnimationFrameDataVector[j].scale[count].x << " "
					<< g_AnimationConverter.AnimationFrameDataVector[j].scale[count].y << " "
					<< g_AnimationConverter.AnimationFrameDataVector[j].scale[count].z << " "
					<< " Quat: " << g_AnimationConverter.AnimationFrameDataVector[j].rotaion[count].x << " "
					<< g_AnimationConverter.AnimationFrameDataVector[j].rotaion[count].y << " "
					<< g_AnimationConverter.AnimationFrameDataVector[j].rotaion[count].z << " "
					<< g_AnimationConverter.AnimationFrameDataVector[j].rotaion[count].w << endl;
				count++;
				// output << "\t\t" << "Time: " << d.keyTime
			}

			output << "\t" << "}" << endl;
		}
		output << "}" << endl;
	}

}

void ConvertBinaryFile(string path)
{
	g_converter.ClearBuffer();
	g_converter.LoadFBX(path.c_str());

	size_t vCount = g_converter.GetVertices().size();
	size_t iCount = g_converter.GetIndices().size();

	string outputName;
	outputName = path;

	for (int i = 0; i < 4; ++i)
	{
		outputName.pop_back();
	}

	outputName += ".bin";
	cout << outputName << endl;

	ofstream output(outputName, ios::binary);

	output.write(CHANGE_BINARY("VertexCount: "), sizeof("VertexCount: "));
	output.write(CHANGE_BINARY(vCount), sizeof(vCount));
	output << endl;

	output.write(CHANGE_BINARY("IndexCount: "), sizeof("IndexCount: "));
	output.write(CHANGE_BINARY(iCount), sizeof(iCount));
	output << endl;

	output.write(CHANGE_BINARY("<VertexList>"), sizeof("<VertexList>"));
	output << endl;
	for (auto& p : g_converter.GetVertices())
	{
		output.write(CHANGE_BINARY(p.Pos.x), sizeof(p.Pos.x));
		output.write(CHANGE_BINARY(" "), sizeof(" "));
		output.write(CHANGE_BINARY(p.Pos.y), sizeof(p.Pos.y));
		output.write(CHANGE_BINARY(" "), sizeof(" "));
		output.write(CHANGE_BINARY(p.Pos.z), sizeof(p.Pos.z));
		output.write(CHANGE_BINARY(" "), sizeof(" "));
	}
	output << endl;

	output.write(CHANGE_BINARY("<NormalList>"), sizeof("<NormalList>"));
	output << endl;
	for (auto& p : g_converter.GetVertices())
	{
		output.write(CHANGE_BINARY(p.Normal.x), sizeof(p.Normal.x));
		output.write(CHANGE_BINARY(" "), sizeof(" "));
		output.write(CHANGE_BINARY(p.Normal.y), sizeof(p.Normal.y));
		output.write(CHANGE_BINARY(" "), sizeof(" "));
		output.write(CHANGE_BINARY(p.Normal.z), sizeof(p.Normal.z));
		output.write(CHANGE_BINARY(" "), sizeof(" "));
	}
	output << endl;

	output.write(CHANGE_BINARY("<TexCList>"), sizeof("<TexCList>"));
	output << endl;
	for (auto& p : g_converter.GetVertices())
	{
		output.write(CHANGE_BINARY(p.TexC.x), sizeof(p.TexC.x));
		output.write(CHANGE_BINARY(" "), sizeof(" "));
		output.write(CHANGE_BINARY(p.TexC.y), sizeof(p.TexC.y));
		output.write(CHANGE_BINARY(" "), sizeof(" "));
	}
	output << endl;

	output.write(CHANGE_BINARY("<IndexList>"), sizeof("<IndexList>"));
	output << endl;
	for (auto& p : g_converter.GetIndices())
	{
		output.write(CHANGE_BINARY(p), sizeof(p));
		output.write(CHANGE_BINARY(" "), sizeof(" "));
	}
	output.close();
}

void IfStreamTest(const char* path)
{
	ifstream fin(path);

	if (!fin)
	{
		cout << "the path not found." << endl;
		return;
	}

	std::string token;
	std::string menu;

	int vCount = 0;
	int iCount = 0;

	fin >> token >> vCount;
	fin >> token >> iCount;

	std::vector<MainVertex> vertices(vCount);
	std::vector<int32_t> indices(iCount);

	while (!fin.eof())
	{
		fin >> token;
		if (token == "<VertexList>")
		{
			for (int i = 0; i < vCount; ++i)
			{
				fin >> vertices[i].Pos.x >> vertices[i].Pos.y >> vertices[i].Pos.z;
			}
		}
		else if (token == "<NormalList>")
		{
			for (int i = 0; i < vCount; ++i)
			{
				fin >> vertices[i].Normal.x >> vertices[i].Normal.y >> vertices[i].Normal.z;
			}
		}
		else if (token == "<TexCList>")
		{
			for (int i = 0; i < vCount; ++i)
			{
				fin >> vertices[i].TexC.x >> vertices[i].TexC.y;
			}
		}
		else if (token == "<IndexList>")
		{
			for (int i = 0; i < iCount; ++i)
			{
				fin >> indices[i];
			}
		}
	}

	fin.close();
}

int main(void)
{
	AnimationConvertFolder("./Ani/");
	// ConvertFolder("./PolygonAdventureTri/");
	// ConvertFolder("./Character/");
	//ConvertFile("SM_Bld_Fence_01.fbx");
	//ConvertBinaryFile("SM_Bld_Fence_01.fbx");
}
