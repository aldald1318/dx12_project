#include "AnimationConverter.h"
#include <algorithm>

FbxAMatrix fbxmtxGeometryOffset;


XMFLOAT4 ToQuaternion(double yaw, double pitch, double roll) // yaw (Z), pitch (Y), roll (X)
{
	// Abbreviations for the various angular functions
	double cy = cos(yaw * 0.5);
	double sy = sin(yaw * 0.5);
	double cp = cos(pitch * 0.5);
	double sp = sin(pitch * 0.5);
	double cr = cos(roll * 0.5);
	double sr = sin(roll * 0.5);

	XMFLOAT4 q;
	q.w = cy * cp * cr + sy * sp * sr;
	q.x = cy * cp * sr - sy * sp * cr;
	q.y = sy * cp * sr + cy * sp * cr;
	q.z = sy * cp * cr - cy * sp * sr;

	return q;
}


FbxAMatrix GetGeometricOffsetTransform(fbxsdk::FbxNode* pfbxNode)
{
	const FbxVector4 T = pfbxNode->GetGeometricTranslation(fbxsdk::FbxNode::eSourcePivot);
	const FbxVector4 R = pfbxNode->GetGeometricRotation(fbxsdk::FbxNode::eSourcePivot);
	const FbxVector4 S = pfbxNode->GetGeometricScaling(fbxsdk::FbxNode::eSourcePivot);

	return(FbxAMatrix(T, R, S));
}

int GetAnimationCurves(fbxsdk::FbxAnimLayer* pfbxAnimationLayer, fbxsdk::FbxNode* pfbxNode)
{
	int nAnimationCurves = 0;

	fbxsdk::FbxAnimCurve* pfbxAnimationCurve = NULL;
	if (pfbxAnimationCurve = pfbxNode->LclTranslation.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_X)) nAnimationCurves++;
	if (pfbxAnimationCurve = pfbxNode->LclTranslation.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_Y)) nAnimationCurves++;
	if (pfbxAnimationCurve = pfbxNode->LclTranslation.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_Z)) nAnimationCurves++;
	if (pfbxAnimationCurve = pfbxNode->LclRotation.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_X)) nAnimationCurves++;
	if (pfbxAnimationCurve = pfbxNode->LclRotation.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_Y)) nAnimationCurves++;
	if (pfbxAnimationCurve = pfbxNode->LclRotation.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_Z)) nAnimationCurves++;
	if (pfbxAnimationCurve = pfbxNode->LclScaling.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_X)) nAnimationCurves++;
	if (pfbxAnimationCurve = pfbxNode->LclScaling.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_Y)) nAnimationCurves++;
	if (pfbxAnimationCurve = pfbxNode->LclScaling.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_Z)) nAnimationCurves++;

	return(nAnimationCurves);
}

int GetAnimationLayerCurveNodes(fbxsdk::FbxAnimLayer* pfbxAnimationLayer, FbxNode* pfbxNode)
{
	int nAnimationCurveNodes = 0;
	if (GetAnimationCurves(pfbxAnimationLayer, pfbxNode) > 0) nAnimationCurveNodes++;

	for (int i = 0; i < pfbxNode->GetChildCount(); i++)
	{
		nAnimationCurveNodes += GetAnimationLayerCurveNodes(pfbxAnimationLayer, pfbxNode->GetChild(i));
	}

	return(nAnimationCurveNodes);
}

vector<float> DisplayCureveTimes(fbxsdk::FbxAnimCurve* pfbxAnimationCurve)
{
	int nKeys = pfbxAnimationCurve->KeyGetCount();

	vector<float> result;
	result.reserve(nKeys);

	for (int i = 0; i < nKeys; i++)
	{
		FbxTime fbxKeyTime = pfbxAnimationCurve->KeyGetTime(i);
		float fkeyTime = (float)fbxKeyTime.GetSecondDouble();
		result.push_back(fkeyTime);
	}

	return result;
}

vector<float> DisplayCurveKeys(char* pHeader, fbxsdk::FbxAnimCurve* pfbxAnimationCurve, char* pSuffix, bool bRotationAngle)
{
	int nKeys = pfbxAnimationCurve->KeyGetCount();

	vector<float> result;
	result.reserve(nKeys);

	for (int i = 0; i < nKeys; i++)
	{
		float fKeyValue = static_cast<float>(pfbxAnimationCurve->KeyGetValue(i));
		if (bRotationAngle) fKeyValue = (3.1415926f / 180.f) * fKeyValue;
		result.push_back(fKeyValue);
	}

	return result;
}


AnimationConverter::AnimationConverter()
{
	InitFBX();

}

AnimationConverter::~AnimationConverter()
{
}

void AnimationConverter::InitFBX()
{
	if (g_pFbxSdkManager == nullptr)
	{
		g_pFbxSdkManager = FbxManager::Create();

		FbxIOSettings* pIOsettings = FbxIOSettings::Create(g_pFbxSdkManager, IOSROOT);
		g_pFbxSdkManager->SetIOSettings(pIOsettings);
	}
}

void AnimationConverter::makeData()
{
	// cout << "animation clip size - " << AnimationFrameDataVector.size() << endl;
	// cout << "BoneOffset size - " << boneOffsets.size() << endl;
	// cout << "HierarchyBone size - " << HierarchyBone.size() << endl;
	// cout << "ClusterBone size - " << ClusterBone.size() << endl;


	// cout << "ClusterBone size = " << ClusterBone.size() << endl;
	//  for (int i = 0; i < ClusterBone.size(); ++i)
	//  	cout << ClusterBone[i] << endl;
	// cout << "==========================================" << endl;
	// cout << "HierarchyBone size = " << HierarchyBone.size() << endl;
	//   for (int i = 0; i < HierarchyBone.size() - 1; ++i)
	//   	cout << i << " - " << HierarchyBone[i] << endl;

	// 어떤 본이 프레임에 없는 지
	// for (int i = 0; i < HierarchyBone.size(); ++i)
	// {
	// 	auto iter = find(ClusterBone.begin(), ClusterBone.end(), HierarchyBone[i]);
	// 	if (iter == ClusterBone.end())
	// 	{
	// 		cout << i << " - " << HierarchyBone[i] << endl;
	// 	}
	// }

	ParentIndex.push_back(-1);
	for (int i = 1; i < HierarchyBoneParent.size() - 1; ++i)
	{
		// cout << HierarchyBone[i] << " - " << HierarchyBoneParent[i] << endl;
		auto iter = find(HierarchyBone.begin(), HierarchyBone.end(), HierarchyBoneParent[i]);
		ParentIndex.push_back(iter - HierarchyBone.begin());
	}

	// for (int i = 0; i < ParentIndex.size(); ++i)
	// {
	// 	cout << ParentIndex[i] << endl;
	// }

	ClusterToHierachtIndexBuffer.reserve(ClusterBone.size() - 1);
	for (int i = 0, j = 0; i < ClusterBone.size(); ++i)
	{
		auto iter = find(HierarchyBone.begin(), HierarchyBone.end(), ClusterBone[i]);
		// cout << iter - HierarchyBone.begin() << endl;
		ClusterToHierachtIndexBuffer.push_back(iter - HierarchyBone.begin());
	}

	// SubSetTable
	// 이거 잘 모르겠어 재질이 두개 이상일때부터
	// 하나 일때도 좀 불안해
	int materialNum = materials.size();

	if (materialNum > 0)
	{
		if (materialNum == 1)
		{
			SubsetTable tmp;
			tmp.subsetID = 0;
			tmp.vertexStart = 0;
			tmp.vertexCount = Controlpoint.size();
			tmp.faceStart = 0;
			tmp.faceCount = Indices.size() / 3;
			subsetTable.push_back(tmp);
		}
		else
		{
			cout << "SubSetTable Error - 재질 두개 이상 사용 ㅜㅜ!" << endl;
		}
	}

	// Vetices;
	AnimationVertex tmpVertex;
	Vertices.reserve(Controlpoint.size());
	for (int i = 0; i < Controlpoint.size(); ++i)
	{
		tmpVertex.Pos = Controlpoint[i];
		tmpVertex.Tangent = Tangent[i];
		tmpVertex.Normal = Normal[i];
		tmpVertex.TexC = UV[i];
		tmpVertex.BlendWeight = boneWeight[i];
		tmpVertex.BlendIndices.x = ClusterToHierachtIndexBuffer[boneIndices[i].x];
		tmpVertex.BlendIndices.y = ClusterToHierachtIndexBuffer[boneIndices[i].y];
		tmpVertex.BlendIndices.z = ClusterToHierachtIndexBuffer[boneIndices[i].z];
		tmpVertex.BlendIndices.w = ClusterToHierachtIndexBuffer[boneIndices[i].w];
		Vertices.push_back(tmpVertex);
	}

	XMFLOAT4X4 iden = {};
	// XMStoreFloat4x4(&iden, XMMatrixIdentity());
	// = XMMatrixIdentity();
	ResultBoneOffsets.resize(HierarchyBone.size());
	for (int i = 0; i < ClusterBone.size(); ++i)
	{
		ResultBoneOffsets[i] = iden;
		// for (int j = 0; j < 4; ++j)
		// 	for (int k = 0; k < 4; ++k)
		// 		ResultBoneOffsets[i].m[j][k] = fbxmtxGeometryOffset[j][k]; // iden;
	}
	for (int i = 0; i < ClusterBone.size(); ++i)
	{
		ResultBoneOffsets[ClusterToHierachtIndexBuffer[i]] = boneOffsets[i];
	}



}

int AnimationConverter::FindHierarchyBoneIndex(string ClusterBoneName)
{
	for (int i = 0; i < ClusterBone.size(); ++i)
	{
		if (ClusterBone[i] == ClusterBoneName)
			return i;
	}

	return -1;
}

bool AnimationConverter::LoadFBX(const char* path)
{
	FbxImporter* pImporter = FbxImporter::Create(g_pFbxSdkManager, "Test Importer");
	bool bSuccess = pImporter->Initialize(path, -1, g_pFbxSdkManager->GetIOSettings());	// SM_Bld_Stall_03	Characters	SM_Prop_Basket_03	SM_Env_Flower_01
	if (!bSuccess) {
		printf("Call to FbxImporter::Initialize() failed.\n");
		printf("Error returned: %s\n\n", pImporter->GetStatus().GetErrorString());
		exit(-1);
	}

	FbxScene* pFbxScene = FbxScene::Create(g_pFbxSdkManager, "Test Scene");

	bSuccess = pImporter->Import(pFbxScene);
	if (!bSuccess) return false;
	pImporter->Destroy();

	FbxNode* pFbxRootNode = pFbxScene->GetRootNode();
	LoadNode(pFbxRootNode);
	// pFbxRootNode->GetChild()->GetGeometricTranslation


	Animation(pFbxScene, pFbxRootNode);

	ReadMaterials(pFbxScene);


	makeData();
	// numMaterial = materials.size();

	// ReadAnimStack(pFbxScene);
	return true;
}

void AnimationConverter::LoadNode(fbxsdk::FbxNode* node)
{
	const int childCount = node->GetChildCount();
	unsigned int vertexCounter = 0;

	if (node)
	{
		if (node->GetNodeAttribute() != NULL)
		{
			fbxsdk::FbxNodeAttribute::EType AttributeType = node->GetNodeAttribute()->GetAttributeType();
			if (AttributeType == fbxsdk::FbxNodeAttribute::eMesh)
			{
				fbxsdk::FbxMesh* pMesh = (FbxMesh*)node->GetNodeAttribute();

				// ReadDeformerInfo(pMesh);


				int nControlPoints = pMesh->GetControlPointsCount();
				if (nControlPoints > 0)
				{
					int nSkinDeformers = pMesh->GetDeformerCount(FbxDeformer::eSkin);
					if (nSkinDeformers > 0)
					{
						// 여기에서 deformer 정보 저장 해줘야해
						ControlPointBoneInfo(pMesh);
					}

					ControlPoint(pMesh);
					ControlPointUVs(pMesh);
					ControlPointNormal(pMesh);
					ControlPointTangent(pMesh);
				}

				int nPolygons = pMesh->GetPolygonCount();
				cout << "Polygon Num : " << nPolygons << endl;
				if (nPolygons > 0) PolygonVertexIndices(pMesh, nPolygons);
			}
		}
	}

	// 다음 child node로 이동
	for (int i = 0; i < childCount; ++i)
		LoadNode(node->GetChild(i));

}

void AnimationConverter::ControlPoint(fbxsdk::FbxMesh* pMesh)
{
	int ControlPointCount = pMesh->GetControlPointsCount();
	Controlpoint.resize(ControlPointCount);

	FbxVector4* pfbxvControlPoints = pMesh->GetControlPoints();
	for (int i = 0; i < ControlPointCount; i++)
	{
		Controlpoint[i].x = pfbxvControlPoints[i][0];
		Controlpoint[i].y = pfbxvControlPoints[i][1];
		Controlpoint[i].z = pfbxvControlPoints[i][2];

		// cout << i << " - " << Controlpoint[i].x << ", " << Controlpoint[i].y << ", " << Controlpoint[i].z << "----" << pfbxvControlPoints[i][4] << endl;
	}
	// cout << "===================================================================================================================" << endl;
}

void AnimationConverter::ControlPointUVs(fbxsdk::FbxMesh* pMesh)
{
	// cout << numVertex << endl;
	int ControlPointCount = pMesh->GetControlPointsCount();
	UV.resize(ControlPointCount);

	int nUVsPerVertex = pMesh->GetElementUVCount(); //UVs Per Polygon's Vertex

	if (nUVsPerVertex > 0)
	{
		int nPolygons = pMesh->GetPolygonCount();
		// for (int k = 0; k < nUVsPerVertex; k++)
		{
			FbxGeometryElementUV* pfbxElementUV = pMesh->GetElementUV(0);

			for (int i = 0; i < nPolygons; i++)
			{
				int nPolygonSize = pMesh->GetPolygonSize(i); //Triangle: 3, Triangulate()
				for (int j = 0; j < nPolygonSize; j++)
				{
					int nControlPointIndex = pMesh->GetPolygonVertex(i, j);
					switch (pfbxElementUV->GetMappingMode())
					{
					case FbxGeometryElement::eByControlPoint:
					{
						switch (pfbxElementUV->GetReferenceMode())
						{
						case FbxGeometryElement::eDirect:
							UV[nControlPointIndex].x = pfbxElementUV->GetDirectArray().GetAt(nControlPointIndex)[0];
							UV[nControlPointIndex].y = pfbxElementUV->GetDirectArray().GetAt(nControlPointIndex)[1];

							break;
						case FbxGeometryElement::eIndexToDirect:
							UV[nControlPointIndex].x = pfbxElementUV->GetDirectArray().GetAt(pfbxElementUV->GetIndexArray().GetAt(nControlPointIndex))[0];
							UV[nControlPointIndex].y = pfbxElementUV->GetDirectArray().GetAt(pfbxElementUV->GetIndexArray().GetAt(nControlPointIndex))[1];

							break;
						default:
							break;
						}
						break;
					}
					case FbxGeometryElement::eByPolygonVertex:
					{
						int nTextureUVIndex = pMesh->GetTextureUVIndex(i, j);
						switch (pfbxElementUV->GetReferenceMode())
						{
						case FbxGeometryElement::eDirect:
						case FbxGeometryElement::eIndexToDirect:
							UV[nControlPointIndex].x = pfbxElementUV->GetDirectArray().GetAt(nTextureUVIndex)[0];
							UV[nControlPointIndex].y = pfbxElementUV->GetDirectArray().GetAt(nTextureUVIndex)[1];

							break;
						default:
							break;
						}
						break;
					}
					case FbxGeometryElement::eByPolygon:
					case FbxGeometryElement::eAllSame:
					case FbxGeometryElement::eNone:
						break;
					default:
						break;
					}
					// cout << nControlPointIndex << " - " << UV[nControlPointIndex].x << ", " << UV[nControlPointIndex].y << endl;
				}
			}
		}
	}
}

void AnimationConverter::ControlPointNormal(fbxsdk::FbxMesh* pMesh)
{
	int ControlPointCount = pMesh->GetControlPointsCount();
	Normal.resize(ControlPointCount);

	int nNormalsPerVertex = pMesh->GetElementNormalCount();
	// cout << nNormalsPerVertex << endl;
	if (nNormalsPerVertex > 0)
	{
		int nPolygons = pMesh->GetPolygonCount();

		for (int k = 0; k < nNormalsPerVertex; k++)
		{
			FbxGeometryElementNormal* pfbxElementNormal = pMesh->GetElementNormal(k);
			if (pfbxElementNormal->GetMappingMode() == FbxGeometryElement::eByControlPoint)
			{
				if (pfbxElementNormal->GetReferenceMode() == FbxGeometryElement::eDirect)
				{
					for (int i = 0; i < ControlPointCount; i++)
					{
						Normal[i].x = pfbxElementNormal->GetDirectArray().GetAt(i)[0];
						Normal[i].y = pfbxElementNormal->GetDirectArray().GetAt(i)[1];
						Normal[i].z = pfbxElementNormal->GetDirectArray().GetAt(i)[2];

						// cout << i << " - " << Normal[i].x << ", " << Normal[i].y << ", " << Normal[i].z << endl;
					}
				}

			}
			else if (pfbxElementNormal->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
			{
				for (int i = 0, nVertexID = 0; i < nPolygons; i++)
				{
					int nPolygonSize = pMesh->GetPolygonSize(i);
					for (int j = 0; j < nPolygonSize; j++, nVertexID++)
					{
						int nControlPointIndex = pMesh->GetPolygonVertex(i, j);
						switch (pfbxElementNormal->GetReferenceMode())
						{
						case FbxGeometryElement::eDirect:
							Normal[nControlPointIndex].x = pfbxElementNormal->GetDirectArray().GetAt(nVertexID)[0];
							Normal[nControlPointIndex].y = pfbxElementNormal->GetDirectArray().GetAt(nVertexID)[1];
							Normal[nControlPointIndex].z = pfbxElementNormal->GetDirectArray().GetAt(nVertexID)[2];

							break;
						case FbxGeometryElement::eIndexToDirect:
							Normal[nControlPointIndex].x = pfbxElementNormal->GetDirectArray().GetAt(pfbxElementNormal->GetIndexArray().GetAt(nVertexID))[0];
							Normal[nControlPointIndex].y = pfbxElementNormal->GetDirectArray().GetAt(pfbxElementNormal->GetIndexArray().GetAt(nVertexID))[1];
							Normal[nControlPointIndex].z = pfbxElementNormal->GetDirectArray().GetAt(pfbxElementNormal->GetIndexArray().GetAt(nVertexID))[2];

							break;
						default:
							break;
						}
						// cout << nControlPointIndex << " - " << Normal[nControlPointIndex].x << ", " << Normal[nControlPointIndex].y << ", " << Normal[nControlPointIndex].z << endl;
					}
				}
			}

		}
	}
}

void AnimationConverter::ControlPointTangent(fbxsdk::FbxMesh* pMesh)
{
	int ControlPointCount = pMesh->GetControlPointsCount();
	Tangent.resize(ControlPointCount);

	int nTangentsPerVertex = pMesh->GetElementTangentCount();
	// cout << nTangentsPerVertex << endl;
	if (nTangentsPerVertex > 0)
	{
		int nPolygons = pMesh->GetPolygonCount();
		for (int k = 0; k < nTangentsPerVertex; k++)
		{
			FbxGeometryElementTangent* pfbxElementTangent = pMesh->GetElementTangent(k);
			if (pfbxElementTangent->GetMappingMode() == FbxGeometryElement::eByControlPoint)
			{
				if (pfbxElementTangent->GetReferenceMode() == FbxGeometryElement::eDirect)
				{
					for (int i = 0; i < ControlPointCount; i++)
					{
						Tangent[i].x = pfbxElementTangent->GetDirectArray().GetAt(i)[0];
						Tangent[i].y = pfbxElementTangent->GetDirectArray().GetAt(i)[1];
						Tangent[i].z = pfbxElementTangent->GetDirectArray().GetAt(i)[2];
						Tangent[i].w = pfbxElementTangent->GetDirectArray().GetAt(i)[3];

					}
				}
			}
			else if (pfbxElementTangent->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
			{
				for (int i = 0, nVertexID = 0; i < nPolygons; i++)
				{
					int nPolygonSize = pMesh->GetPolygonSize(i);
					for (int j = 0; j < nPolygonSize; j++, nVertexID++)
					{
						int nControlPointIndex = pMesh->GetPolygonVertex(i, j);
						switch (pfbxElementTangent->GetReferenceMode())
						{
						case FbxGeometryElement::eDirect:
							Tangent[nControlPointIndex].x = pfbxElementTangent->GetDirectArray().GetAt(nVertexID)[0];
							Tangent[nControlPointIndex].y = pfbxElementTangent->GetDirectArray().GetAt(nVertexID)[1];
							Tangent[nControlPointIndex].z = pfbxElementTangent->GetDirectArray().GetAt(nVertexID)[2];

							break;
						case FbxGeometryElement::eIndexToDirect:
							Tangent[nControlPointIndex].x = pfbxElementTangent->GetDirectArray().GetAt(pfbxElementTangent->GetIndexArray().GetAt(nVertexID))[0];
							Tangent[nControlPointIndex].y = pfbxElementTangent->GetDirectArray().GetAt(pfbxElementTangent->GetIndexArray().GetAt(nVertexID))[1];
							Tangent[nControlPointIndex].z = pfbxElementTangent->GetDirectArray().GetAt(pfbxElementTangent->GetIndexArray().GetAt(nVertexID))[2];
							break;
						default:
							break;
						}
					}
				}
			}
			// 아래 for문이 여기 있으면 tangent가 있을 경우에만 출력
		}
	}
	// for (int i = 0; i < Tangent.size(); ++i)
	// 	cout << i << " - " << Tangent[i].x << ", " << Tangent[i].y << ", " << Tangent[i].z << endl;
}

void AnimationConverter::ControlPointBoneInfo(fbxsdk::FbxMesh* pMesh)
{
	FbxSkin* pfbxSkinDeformer = (FbxSkin*)pMesh->GetDeformer(0, FbxDeformer::eSkin);
	int nClusters = pfbxSkinDeformer->GetClusterCount();


	cout << "ClusterNum - " << nClusters << endl;
	// for (int j = 0; j < nClusters; j++)
	// {
	// 	FbxCluster* pfbxCluster = pfbxSkinDeformer->GetCluster(j);
	// 
	// 	FbxNode* pfbxClusterLinkNode = pfbxCluster->GetLink();
	// 	ClusterBone.push_back(pfbxClusterLinkNode->GetName());
	// }
	// cout << boneName.size() << endl;
	// 뼈 이름 출력해보기
	// for (int i = 0; i < nClusters; ++i)
	// {
	// 	cout << i << " - " << boneName[i] << endl;
	// }

	boneOffsets.resize(nClusters);
	fbxmtxGeometryOffset = GetGeometricOffsetTransform(pMesh->GetNode());
	for (int j = 0; j < nClusters; j++)
	{
		FbxCluster* pfbxCluster = pfbxSkinDeformer->GetCluster(j);

		ClusterBone.push_back(pfbxCluster->GetLink()->GetName());
		// cout << pfbxCluster->GetLink()->GetName() << endl;
		// ParentBoneName.push_back(pfbxCluster->GetLink()->GetParent()->GetName());
		// cout << pfbxCluster->GetLink()->GetParent()->GetName() << endl;


		FbxAMatrix fbxmtxBindPoseMeshToRoot; //Cluster Transform
		pfbxCluster->GetTransformMatrix(fbxmtxBindPoseMeshToRoot);
		FbxAMatrix fbxmtxBindPoseBoneToRoot; //Cluster Link Transform
		pfbxCluster->GetTransformLinkMatrix(fbxmtxBindPoseBoneToRoot);


		FbxAMatrix fbxmtxVertextToLinkNode = fbxmtxBindPoseBoneToRoot.Inverse() * fbxmtxBindPoseMeshToRoot * fbxmtxGeometryOffset;

		XMFLOAT4X4 tmp;
		for (int a = 0; a < 4; ++a)
		{
			for (int b = 0; b < 4; ++b)
			{
				tmp.m[a][b] = (float)fbxmtxVertextToLinkNode[a][b];
			}
		}
		boneOffsets[j] = tmp;

		// cout << boneOffsets[j].m[0][0] << "\t" << boneOffsets[j].m[0][1] << "\t" << boneOffsets[j].m[0][2] << endl;
		// cout << boneOffsets[j].m[1][0] << "\t" << boneOffsets[j].m[1][1] << "\t" << boneOffsets[j].m[1][2] << endl;
		// cout << boneOffsets[j].m[2][0] << "\t" << boneOffsets[j].m[2][1] << "\t" << boneOffsets[j].m[2][2] << endl;
	}


	int nControlPoints = pMesh->GetControlPointsCount();

	int* pnBonesPerVertex = new int[nControlPoints];
	::memset(pnBonesPerVertex, 0, nControlPoints * sizeof(int));

	for (int j = 0; j < nClusters; j++)
	{
		FbxCluster* pfbxCluster = pfbxSkinDeformer->GetCluster(j);

		int nControlPointIndices = pfbxCluster->GetControlPointIndicesCount();
		int* pnControlPointIndices = pfbxCluster->GetControlPointIndices();
		for (int k = 0; k < nControlPointIndices; k++) pnBonesPerVertex[pnControlPointIndices[k]] += 1;
	}

	int** ppnBoneIDs = new int* [nControlPoints];
	float** ppnBoneWeights = new float* [nControlPoints];
	for (int i = 0; i < nControlPoints; i++)
	{
		ppnBoneIDs[i] = new int[pnBonesPerVertex[i]];
		ppnBoneWeights[i] = new float[pnBonesPerVertex[i]];
		::memset(ppnBoneIDs[i], 0, pnBonesPerVertex[i] * sizeof(int));
		::memset(ppnBoneWeights[i], 0, pnBonesPerVertex[i] * sizeof(float));
	}
	int* pnBones = new int[nControlPoints];
	::memset(pnBones, 0, nControlPoints * sizeof(int));

	for (int j = 0; j < nClusters; j++)
	{
		FbxCluster* pfbxCluster = pfbxSkinDeformer->GetCluster(j);

		int* pnControlPointIndices = pfbxCluster->GetControlPointIndices();
		double* pfControlPointWeights = pfbxCluster->GetControlPointWeights();
		int nControlPointIndices = pfbxCluster->GetControlPointIndicesCount();

		for (int k = 0; k < nControlPointIndices; k++)
		{
			int nVertex = pnControlPointIndices[k];
			ppnBoneIDs[nVertex][pnBones[nVertex]] = j;
			ppnBoneWeights[nVertex][pnBones[nVertex]] = (float)pfControlPointWeights[k];
			pnBones[nVertex] += 1;
		}
	}

	for (int i = 0; i < nControlPoints; i++)
	{
		for (int j = 0; j < pnBonesPerVertex[i] - 1; j++)
		{
			for (int k = j + 1; k < pnBonesPerVertex[i]; k++)
			{
				if (ppnBoneWeights[i][j] < ppnBoneWeights[i][k])
				{
					float fTemp = ppnBoneWeights[i][j];
					ppnBoneWeights[i][j] = ppnBoneWeights[i][k];
					ppnBoneWeights[i][k] = fTemp;
					int nTemp = ppnBoneIDs[i][j];
					ppnBoneIDs[i][j] = ppnBoneIDs[i][k];
					ppnBoneIDs[i][k] = nTemp;
				}
			}
		}
	}

	int(*pnSkinningIndices)[4] = new int[nControlPoints][4];
	float(*pfSkinningWeights)[4] = new float[nControlPoints][4];

	for (int i = 0; i < nControlPoints; i++)
	{
		::memset(pnSkinningIndices[i], 0, 4 * sizeof(int));
		::memset(pfSkinningWeights[i], 0, 4 * sizeof(float));

		for (int j = 0; j < pnBonesPerVertex[i]; j++)
		{
			if (j < 4)
			{
				pnSkinningIndices[i][j] = ppnBoneIDs[i][j];
				pfSkinningWeights[i][j] = ppnBoneWeights[i][j];
			}
		}
	}

	for (int i = 0; i < nControlPoints; i++)
	{
		float nSumOfBoneWeights = 0.0f;
		for (int j = 0; j < 4; j++) nSumOfBoneWeights += pfSkinningWeights[i][j];
		for (int j = 0; j < 4; j++) pfSkinningWeights[i][j] /= nSumOfBoneWeights;
	}

	boneIndices.resize(nControlPoints);
	boneWeight.resize(nControlPoints);
	for (int i = 0; i < nControlPoints; ++i)
	{
		boneIndices[i] = { pnSkinningIndices[i][0],  pnSkinningIndices[i][1], pnSkinningIndices[i][2], pnSkinningIndices[i][3] };
		boneWeight[i] = { pfSkinningWeights[i][0], pfSkinningWeights[i][1], pfSkinningWeights[i][2], pfSkinningWeights[i][3] };
		// if (boneIndices[i].x > 44 || boneIndices[i].y > 44 || boneIndices[i].z > 44 || boneIndices[i].w > 44)
		//	 cout << boneIndices[i].x << ", " << boneIndices[i].y << ", " << boneIndices[i].z << ", " << boneIndices[i].w << endl;
		// cout << boneWeight[i].x << ", " << boneWeight[i].y << ", " << boneWeight[i].z << ", " << boneWeight[i].w << endl;
	}


	for (int i = 0; i < nControlPoints; i++)
	{
		if (ppnBoneIDs[i]) delete[] ppnBoneIDs[i];
		if (ppnBoneWeights[i]) delete[] ppnBoneWeights[i];
	}
	if (ppnBoneIDs) delete[] ppnBoneIDs;
	if (ppnBoneWeights) delete[] ppnBoneWeights;

	if (pnBones) delete[] pnBones;
	if (pnBonesPerVertex) delete[] pnBonesPerVertex;
	if (pnSkinningIndices) delete[] pnSkinningIndices;
	if (pfSkinningWeights) delete[] pfSkinningWeights;

}

void AnimationConverter::PolygonVertexIndices(fbxsdk::FbxMesh* pfbxMesh, int nPolygons)
{
	int nPolygonIndices = nPolygons * 3; // 인덱스 개수

	int* pnPolygonIndices = new int[nPolygonIndices];
	for (int i = 0, k = 0; i < nPolygons; i++)
	{
		for (int j = 0; j < 3; j++) pnPolygonIndices[k++] = pfbxMesh->GetPolygonVertex(i, j);
	}

	FbxNode* pfbxNode = pfbxMesh->GetNode();
	int nMaterials = pfbxNode->GetMaterialCount();	// material 개수

	if (nMaterials > 1)
	{
		int nElementMaterials = pfbxMesh->GetElementMaterialCount();
		for (int i = 0; i < nElementMaterials; i++)
		{
			FbxGeometryElementMaterial* pfbxElementMaterial = pfbxMesh->GetElementMaterial(i);
			FbxGeometryElement::EReferenceMode nReferenceMode = pfbxElementMaterial->GetReferenceMode();
			switch (nReferenceMode)
			{
			case FbxGeometryElement::eDirect:
			{
				cout << "index count : " << nPolygonIndices << endl;
				for (int i = 0; i < nPolygonIndices; i++)
				{
					Indices.push_back(pnPolygonIndices[i]);
				}
				break;
			}
			case FbxGeometryElement::eIndex:
			case FbxGeometryElement::eIndexToDirect:
			{
				int* pnSubIndices = new int[nMaterials];
				memset(pnSubIndices, 0, sizeof(int) * nMaterials);

				int nSubIndices = pfbxElementMaterial->GetIndexArray().GetCount();
				for (int j = 0; j < nSubIndices; j++) pnSubIndices[pfbxElementMaterial->GetIndexArray().GetAt(j)]++;

				int** ppnSubIndices = new int* [nMaterials];
				for (int k = 0; k < nMaterials; k++)
				{
					pnSubIndices[k] *= 3;
					ppnSubIndices[k] = new int[pnSubIndices[k]];
				}

				int* pnToAppends = new int[nMaterials];
				memset(pnToAppends, 0, sizeof(int) * nMaterials);
				for (int j = 0, k = 0; j < nSubIndices; j++)
				{
					int nMaterial = pfbxElementMaterial->GetIndexArray().GetAt(j);
					for (int i = 0; i < 3; i++) ppnSubIndices[nMaterial][pnToAppends[nMaterial]++] = pnPolygonIndices[k++];
				}

				for (int k = 0; k < nMaterials; k++)
				{
					// DisplayInt("<SubIndex>: ", k, pnSubIndices[k], "", nTabIndents + 1);
					cout << "Index Count : " << pnSubIndices[k] << endl;
					for (int j = 0; j < pnSubIndices[k]; j++)
					{
						Indices.push_back(ppnSubIndices[k][j]);
					}
				}

				if (pnSubIndices) delete[] pnSubIndices;
				for (int k = 0; k < nMaterials; k++) if (ppnSubIndices[k]) delete[] ppnSubIndices[k];
				if (ppnSubIndices) delete[] ppnSubIndices;
				if (pnToAppends) delete[] pnToAppends;

				break;
			}
			}
		}
	}
	else
	{
		cout << "Index Count : " << nPolygonIndices << endl;
		for (int i = 0; i < nPolygonIndices; i++)
		{
			Indices.push_back(pnPolygonIndices[i]);
		}
	}

	if (pnPolygonIndices) delete[] pnPolygonIndices;

	// for (int i = 0; i < Indices.size(); ++i)
	// 	cout << Indices[i] << "\t";
	// cout << endl;
	// cout << "==========================================================================================" << endl;
	// cout << "==========================================================================================" << endl;
	// cout << "==========================================================================================" << endl;

}

void AnimationConverter::Animation(fbxsdk::FbxScene* pScenes, fbxsdk::FbxNode* pNode)
{
	int nAnimationStacks = pScenes->GetSrcObjectCount<fbxsdk::FbxAnimStack>();


	animationName.resize(nAnimationStacks);
	for (int i = 0; i < nAnimationStacks; i++)
	{
		fbxsdk::FbxAnimStack* pfbxAnimStack = pScenes->GetSrcObject<fbxsdk::FbxAnimStack>(i);
		string tmp = pfbxAnimStack->GetName();
		tmp.erase(remove(tmp.begin(), tmp.end(), ' '), tmp.end());

		// tmp.erase(std::remove(tmp.begin(), tmp.end(), ' '), tmp.end());
		animationName[i] = tmp;

		// 현재 애니메이션의 시작 시간과 끝나는 시간(프레임)
		// FbxTime fbxTimeStart = pfbxAnimStack->LocalStart;
		// FbxTime fbxTimeStop = pfbxAnimStack->LocalStop;

		DisplayAnimation(pfbxAnimStack, pScenes->GetRootNode());
	}
}

void AnimationConverter::DisplayAnimation(FbxAnimStack* pfbxAnimStack, FbxNode* pfbxNode)
{
	int nAnimationLayers = pfbxAnimStack->GetMemberCount<fbxsdk::FbxAnimLayer>();

	for (int i = 0; i < nAnimationLayers; i++)
	{
		fbxsdk::FbxAnimLayer* pfbxAnimationLayer = pfbxAnimStack->GetMember<fbxsdk::FbxAnimLayer>(i);
		int nLayerCurveNodes = GetAnimationLayerCurveNodes(pfbxAnimationLayer, pfbxNode);


		int nCurveNode = 0;
		DisplayAnimation(pfbxAnimationLayer, pfbxNode, &nCurveNode);
	}

}

void AnimationConverter::DisplayAnimation(FbxAnimLayer* pfbxAnimationLayer, FbxNode* pfbxNode, int* pnCurveNode)
{
	Channel(pfbxAnimationLayer, pfbxNode, pnCurveNode);

	for (int i = 0; i < pfbxNode->GetChildCount(); i++)
	{
		DisplayAnimation(pfbxAnimationLayer, pfbxNode->GetChild(i), pnCurveNode);
	}

}

void AnimationConverter::Channel(fbxsdk::FbxAnimLayer* pfbxAnimationLayer, FbxNode* pfbxNode, int* pnCurveNode)
{
	//
	if (GetAnimationCurves(pfbxAnimationLayer, pfbxNode) > 0)
	{
		fbxsdk::FbxAnimCurve* pfbxAnimationCurve = NULL;

		vector<float> Time[9];
		vector<float> PosX;
		vector<float> PosY;
		vector<float> PosZ;
		vector<float> RotX;
		vector<float> RotY;
		vector<float> RotZ;
		vector<float> ScaleX;
		vector<float> ScaleY;
		vector<float> ScaleZ;

		// 게층 구조에 맞는 뼈의 이름을 얻을 수 있음
		// cout << pfbxNode->GetName() << endl;
		HierarchyBone.push_back(pfbxNode->GetName());
		HierarchyBoneParent.push_back(pfbxNode->GetParent()->GetName());
		// cout << "HierarchyBone Size = " << HierarchyBone.size() << endl;

		FbxDouble3 d = pfbxNode->LclTranslation.Get();
		string name = pfbxNode->GetName();

		pfbxAnimationCurve = pfbxNode->LclTranslation.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_X); //"X"
		if (pfbxAnimationCurve)
		{
			Time[0] = DisplayCureveTimes(pfbxAnimationCurve);
			PosX = DisplayCurveKeys("<TX>: ", pfbxAnimationCurve, "\n", false);
		}

		pfbxAnimationCurve = pfbxNode->LclTranslation.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_Y);
		if (pfbxAnimationCurve)
		{
			Time[1] = DisplayCureveTimes(pfbxAnimationCurve);
			PosY = DisplayCurveKeys("<TY>: ", pfbxAnimationCurve, "\n", false);
		}

		pfbxAnimationCurve = pfbxNode->LclTranslation.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_Z);
		if (pfbxAnimationCurve)
		{
			Time[2] = DisplayCureveTimes(pfbxAnimationCurve);
			PosZ = DisplayCurveKeys("<TZ>: ", pfbxAnimationCurve, "\n", false);
		}

		pfbxAnimationCurve = pfbxNode->LclRotation.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_X);
		if (pfbxAnimationCurve)
		{
			Time[3] = DisplayCureveTimes(pfbxAnimationCurve);
			RotX = DisplayCurveKeys("<RX>: ", pfbxAnimationCurve, "\n", true);
		}

		pfbxAnimationCurve = pfbxNode->LclRotation.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_Y);
		if (pfbxAnimationCurve)
		{
			Time[4] = DisplayCureveTimes(pfbxAnimationCurve);
			RotY = DisplayCurveKeys("<RY>: ", pfbxAnimationCurve, "\n", true);
		}

		pfbxAnimationCurve = pfbxNode->LclRotation.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_Z);
		if (pfbxAnimationCurve)
		{
			Time[5] = DisplayCureveTimes(pfbxAnimationCurve);
			RotZ = DisplayCurveKeys("<RZ>: ", pfbxAnimationCurve, "\n", true);
		}

		pfbxAnimationCurve = pfbxNode->LclScaling.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_X);
		if (pfbxAnimationCurve)
		{
			Time[6] = DisplayCureveTimes(pfbxAnimationCurve);
			ScaleX = DisplayCurveKeys("<SX>: ", pfbxAnimationCurve, "\n", false);
		}

		pfbxAnimationCurve = pfbxNode->LclScaling.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_Y);
		if (pfbxAnimationCurve)
		{
			Time[7] = DisplayCureveTimes(pfbxAnimationCurve);
			ScaleY = DisplayCurveKeys("<SY>: ", pfbxAnimationCurve, "\n", false);
		}

		pfbxAnimationCurve = pfbxNode->LclScaling.GetCurve(pfbxAnimationLayer, FBXSDK_CURVENODE_COMPONENT_Z);
		if (pfbxAnimationCurve)
		{
			Time[8] = DisplayCureveTimes(pfbxAnimationCurve);
			ScaleZ = DisplayCurveKeys("<SZ>: ", pfbxAnimationCurve, "\n", false);
		}



		vector<int> size;
		// for (int i = 0; i < 9; ++i)
		// 	size.push_back(Time[i].size());
		size.push_back(PosX.size());
		size.push_back(PosY.size());
		size.push_back(PosZ.size());
		size.push_back(ScaleX.size());
		size.push_back(ScaleY.size());
		size.push_back(ScaleZ.size());
		size.push_back(RotX.size());
		size.push_back(RotY.size());
		size.push_back(RotZ.size());

		int maxIndex = size[0];
		for (int i = 0; i < size.size(); ++i)
		{
			if (maxIndex < size[i])
				maxIndex = size[i];
		}

		vector<float> resultScaleX;
		vector<float> resultScaleY;
		vector<float> resultScaleZ;

		for (int i = 0; i < maxIndex; ++i)
		{
			resultScaleX.push_back(1.f);
			resultScaleY.push_back(1.f);
			resultScaleZ.push_back(1.f);
		}
		for (int i = 0; i < ScaleX.size(); ++i)
		{
			resultScaleX[i] = ScaleX[i];
			resultScaleY[i] = ScaleY[i];
			resultScaleZ[i] = ScaleZ[i];

		}

		// for (int i = 0; i < 9; ++i)
		// 	if (maxIndex > Time[i].size()) Time[i].resize(maxIndex);
		if (maxIndex > PosX.size()) PosX.resize(maxIndex);
		if (maxIndex > PosY.size()) PosY.resize(maxIndex);
		if (maxIndex > PosZ.size()) PosZ.resize(maxIndex);
		// if (maxIndex > ScaleX.size()) ScaleX.resize(maxIndex);
		// if (maxIndex > ScaleY.size()) ScaleY.resize(maxIndex);
		// if (maxIndex > ScaleZ.size()) ScaleZ.resize(maxIndex);
		if (maxIndex > RotX.size()) RotX.resize(maxIndex);
		if (maxIndex > RotY.size()) RotY.resize(maxIndex);
		if (maxIndex > RotZ.size()) RotZ.resize(maxIndex);

		int maxTimeIndex = 0;
		int maxTimeSize = Time[maxTimeIndex].size();
		for (int i = 1; i < 9; ++i)
		{
			if (Time[i].size() > maxTimeSize)
				maxTimeIndex = i;
		}
		for (int i = 0; i < 9; ++i)
		{
			if (maxTimeSize >= Time[i].size())
			{
				Time[i].resize(maxTimeSize);
				Time[i] = Time[maxTimeIndex];
			}
		}

		AnimationFrameData tempData;
		for (int i = 0; i < Time[0].size(); ++i)
		{
			XMFLOAT3 tempFloat3;

			tempData.keyTime.push_back(Time[0][i]);

			tempFloat3.x = PosX[i];
			tempFloat3.y = PosY[i];
			tempFloat3.z = PosZ[i];
			tempData.position.push_back(tempFloat3);

			tempFloat3.x = resultScaleX[i];
			tempFloat3.y = resultScaleY[i];
			tempFloat3.z = resultScaleZ[i];
			tempData.scale.push_back(tempFloat3);

			tempFloat3.x = RotX[i];
			tempFloat3.y = RotY[i];
			tempFloat3.z = RotZ[i];
			XMFLOAT4 QuaRot;
			XMStoreFloat4(&QuaRot, DirectX::XMQuaternionRotationRollPitchYawFromVector(XMVECTOR{ tempFloat3.x, tempFloat3.y,tempFloat3.z }));

			// QuaRot = ToQuaternion(tempFloat3.y, tempFloat3.x, tempFloat3.z);

			tempData.rotaion.push_back(QuaRot);
		}
		AnimationFrameDataVector.push_back(tempData);
		AnimationFrameNum.push_back(Time[0].size());
		// cout << AnimationFrameDataVector.size() << endl;
	}
}


void AnimationConverter::ReadMaterials(FbxScene* pFbxScene)
{
	int numMaterial = pFbxScene->GetMaterialCount();
	cout << "numMaterial: " << numMaterial << endl;

	for (int i = 0; i < numMaterial; ++i)
	{
		fbxsdk::FbxSurfaceMaterial* fbxMaterial = pFbxScene->GetMaterial(i);
		FbxProperty prop;
		// if(fbxMaterial->GetClassId().Is())
		// fbxMaterial->GetClassId()
		// FbxSurfacePhong* phongMaterial = (FbxSurfacePhong*)fbxMaterial;
		fbxsdk::FbxSurfacePhong* phongMaterial = (fbxsdk::FbxSurfacePhong*)fbxMaterial;

		Material tmpMaterial;
		// std::cout << tmpMaterial.name << endl;
		tmpMaterial.diffuse.x = phongMaterial->Diffuse.Get().mData[0];
		tmpMaterial.diffuse.y = phongMaterial->Diffuse.Get().mData[1];
		tmpMaterial.diffuse.z = phongMaterial->Diffuse.Get().mData[2];

		tmpMaterial.materialTypeName = "Skinned";

		materials.push_back(tmpMaterial);
	}
}

